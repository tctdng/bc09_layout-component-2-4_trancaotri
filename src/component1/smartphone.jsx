import React, { Component } from "react";
import iphoneX from "../assets/img/sp_iphoneX.png";
import blackBerry from "../assets/img/sp_blackberry.png";
import note7 from "../assets/img/sp_note7.png";
import vivo850 from "../assets/img/sp_vivo850.png";

class Smartphone extends Component {
  dataDT = [
    {
      hinhAnh: iphoneX,
      tenDT: "IPHONE X",
      moTa: "iPhone X features a new all-screen design. Face ID, which makes your face your password",
    },
    {
      hinhAnh: blackBerry,
      tenDT: "Blackerry",
      moTa: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, dicta!",
    },
    {
      hinhAnh: note7,
      tenDT: "Samsung Note7",
      moTa: "Samsung Note7 features a new all-screen design. Face ID, which makes your face your password",
    },
    {
      hinhAnh: vivo850,
      tenDT: "Vivo 850",
      moTa: "Vivo 850 features a new all-screen design. Face ID, which makes your face your password",
    },
  ];
  renderDT = () => {
    const dienthoaiHTML = this.dataDT.map((item) => {
      return (
        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-lg-3">
          <div className="container">
            <div className="card bg-light" style={{ width: 300 }}>
              <img
                className="card-img-top"
                src={item.hinhAnh}
                alt="Card image"
                style={{ maxWidth: "100%", height: 250 }}
              />
              <div className="card-body text-center">
                <h4 className="card-title text-center">{item.tenDT}</h4>
                <p className="card-text">{item.moTa}</p>
                <a href="#" className="btn btn-primary">
                  Detail
                </a>
                <a href="#" className="btn btn-danger">
                  Cart
                </a>
              </div>
            </div>
          </div>
        </div>
      );
    });
    return dienthoaiHTML;
  };

  render() {
    return (
      <div>
        <div id="smartphone" className="container-fluid pt-5 pb-5 bg-dark">
          <h1 className="text-white text-center">BEST SMARTPHONE</h1>
          <div className="row">{this.renderDT()}</div>
        </div>
      </div>
    );
  }
}

export default Smartphone;
