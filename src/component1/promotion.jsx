import React, { Component } from "react";
import promotion1 from "../assets/img/promotion_1.png";
import promotion2 from "../assets/img/promotion_2.png";
import promotion3 from "../assets/img/promotion_3.jpg";

class Promotion extends Component {
  data = [
    {
      hinhAnh: promotion1,
    },
    {
      hinhAnh: promotion2,
    },
    {
      hinhAnh: promotion3,
    },
  ];

  renderPro = () => {
    const proHTML = this.data.map((item) => {
      return (
        <div className="col-xs-12 col-sm-12 col-md-4">
          <div className="container">
            <img src={item.hinhAnh} alt style={{ maxWidth: "100%" }} />
          </div>
        </div>
      );
    });
    return proHTML;
  };

  render() {
    return (
      <div>
        <div id="promotion" className="container-fluid pt-5 pb-5 bg-dark">
          <h1 className="text-white text-center">PROMOTION</h1>
          <div className="container bg-light pt-5 pb-5">
            <div className="row">{this.renderPro()}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Promotion;
