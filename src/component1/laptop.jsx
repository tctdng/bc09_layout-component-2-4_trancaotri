import React, { Component } from "react";
import laptopHp from "../assets/img/lt_hp.png";
import laptopLenovo from "../assets/img/lt_lenovo.png";
import laptopMacbook from "../assets/img/lt_macbook.png";
import laptopRog from "../assets/img/lt_rog.png";

class Laptop extends Component {
  dataLaptop = [
    {
      hinhAnh: laptopHp,
      tenLaptop: "laptop HP",
      moTa: "laptop HP features a new all-screen design. Face ID, which makes your face your password",
    },
    {
      hinhAnh: laptopLenovo,
      tenLaptop: "laptop Lenovo",
      moTa: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, dicta!",
    },
    {
      hinhAnh: laptopMacbook,
      tenLaptop: "Macbook pro",
      moTa: "macbook pro features a new all-screen design. Face ID, which makes your face your password",
    },
    {
      hinhAnh: laptopRog,
      tenLaptop: "ROG",
      moTa: "laptop rog features a new all-screen design. Face ID, which makes your face your password",
    },
  ];

  renderLaptop = () => {
    const laptopHTML = this.dataLaptop.map((item) => {
      return (
        <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-lg-3">
          <div className="container">
            <div className="card bg-light" style={{ width: 300 }}>
              <img
                className="card-img-top"
                src={item.hinhAnh}
                alt="Card image"
                style={{ maxWidth: "100%", height: 250 }}
              />
              <div className="card-body text-center">
                <h4 className="card-title text-center">{item.tenLaptop}</h4>
                <p className="card-text">{item.moTa}</p>
                <a href="#" className="btn btn-primary">
                  Detail
                </a>
                <a href="#" className="btn btn-danger">
                  Cart
                </a>
              </div>
            </div>
          </div>
        </div>
      );
    });
    return laptopHTML;
  };

  render() {
    return (
      <div>
        <div id="laptop" className="container-fluid pt-5 pb-5 bg-white">
          <h1 className="text-white text-center">BEST LAPTOP</h1>
          <div className="row">{this.renderLaptop()}</div>
        </div>
      </div>
    );
  }
}

export default Laptop;
