import React, { Component } from "react";
import Header from "./header";
import Carousel from "./carousel";
import Conntent from "./conntent";
import Footer from "./footer";

class Baitap4 extends Component {
  render() {
    return (
      <div>
        <Header />
        <Carousel />
        <Conntent />
        <Footer />
      </div>
    );
  }
}

export default Baitap4;
